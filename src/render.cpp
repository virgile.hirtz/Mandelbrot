#include "render.hpp"
#include <cstdint>
#include <cassert>

#include <vector>
#include <numeric>
#include <list>

#include "tbb/tbb.h"

#include <immintrin.h>

#include <iostream>


struct rgb8_t {
  std::uint8_t r;
  std::uint8_t g;
  std::uint8_t b;
};

rgb8_t heat_lut(float x)
{
  assert(0 <= x && x <= 1);
  float x0 = 1.f / 4.f;
  float x1 = 2.f / 4.f;
  float x2 = 3.f / 4.f;

  if (x < x0)
  {
    auto g = static_cast<std::uint8_t>(x / x0 * 255);
    return rgb8_t{0, g, 255};
  }
  else if (x < x1)
  {
    auto b = static_cast<std::uint8_t>((x1 - x) / x0 * 255);
    return rgb8_t{0, 255, b};
  }
  else if (x < x2)
  {
    auto r = static_cast<std::uint8_t>((x - x1) / x0 * 255);
    return rgb8_t{r, 255, 0};
  }
  else
  {
    auto b = static_cast<std::uint8_t>((1.f - x) / x0 * 255);
    return rgb8_t{255, b, 0};
  }
}

void render(std::byte* buffer,
            int width,
            int height,
            std::ptrdiff_t stride,
            int n_iterations)
{
  auto iter_mem = std::vector<int>((height + 1) / 2 * width);
  auto histogram = std::vector<int>(n_iterations + 1, 0);

  int* __restrict__ iter_mem_buffer = &iter_mem[0];

  // Round width mutliple of 8
  int roundedWidth = (width + 7) & ~7UL; 

  // Const declarations
  const float scale_x = 3.5f / (float) (width-1);
  const float scale_y = 2.0f / (float) (height-1);
  constexpr float x1 = -2.5f;
  constexpr float y1 = -1.0f;
  constexpr float increment = 1.0f;
  constexpr float comparison = 4.0f;

  // Const broadcast
  __m256 v_scale_x = _mm256_broadcast_ss(&scale_x);
  __m256 v_scale_y = _mm256_broadcast_ss(&scale_y);
  __m256 v_x1 = _mm256_broadcast_ss(&x1);
  __m256 v_y1 = _mm256_broadcast_ss(&y1);
  __m256 v_1f = _mm256_broadcast_ss(&increment);
  __m256 v_4f = _mm256_broadcast_ss(&comparison);

  float incr[8] = {0.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f};
  __m256 v_line = _mm256_xor_ps(v_scale_x, v_scale_x);

  for (int y_ = 0; y_ < (height + 1) / 2; ++y_)
  {
    __m256 v_x_increment = _mm256_load_ps(incr);
    for (int x_ = 0; x_ < roundedWidth; x_ += 8)
    {
      __m256 v_x0 = _mm256_mul_ps(v_scale_x, v_x_increment);
      v_x0 = _mm256_add_ps(v_x1, v_x0);
      __m256 v_y0 = _mm256_mul_ps(v_scale_y, v_line);
      v_y0 = _mm256_add_ps(v_y1, v_y0);

      __m256 v_iteration_counter = _mm256_xor_ps(v_scale_x, v_scale_x);
      __m256 v_xi = v_iteration_counter; // Initial xi = 0
      __m256 v_yi = v_iteration_counter; // Initial yi = 0

      int iteration = 0;
      unsigned int test = 1; // Non null value

      __m256 squared_y = _mm256_mul_ps(v_y0, v_y0);

      __m256 x_minus_quarter = _mm256_div_ps(v_1f, v_4f);
      x_minus_quarter = _mm256_sub_ps(v_x0, x_minus_quarter);

      __m256 tmp = _mm256_mul_ps(x_minus_quarter, x_minus_quarter);

      // Black circles detection
      __m256 q = _mm256_add_ps(tmp, squared_y);

      tmp = _mm256_add_ps(v_x0, v_1f);
      tmp = _mm256_mul_ps(tmp, tmp);
      tmp = _mm256_add_ps(tmp, squared_y);

      __m256 tmp1 = _mm256_mul_ps(v_4f, v_4f);
      tmp1 = _mm256_div_ps(v_1f, tmp1);

      __m256 detect = _mm256_cmp_ps(tmp, tmp1, _CMP_LT_OQ);
      detect = _mm256_and_ps(detect, v_1f);

      tmp = _mm256_add_ps(x_minus_quarter, q);
      tmp = _mm256_mul_ps(tmp, q);

      tmp1 = _mm256_div_ps(squared_y, v_4f);

      __m256 detect1 = _mm256_cmp_ps(tmp, tmp1, _CMP_LT_OQ);
      detect1 = _mm256_and_ps(detect1, v_1f);

      detect = _mm256_or_ps(detect, detect1);

      float detect_store[8];
      _mm256_store_ps(detect_store, detect);
      int mem = 0;
      for (int i = 0; i < 8; i++)
        mem += detect_store[i];
      
      const float float_iter = n_iterations;

      if (mem == 8)
      {
        v_iteration_counter = _mm256_mul_ps(_mm256_broadcast_ss(&float_iter), detect);
        iteration = n_iterations;
      }

      while (test != 0 && iteration < n_iterations)
      {
        __m256 v_xi_squared = _mm256_mul_ps(v_xi, v_xi);
        __m256 v_yi_squared = _mm256_mul_ps(v_yi, v_yi);

        __m256 v_xyi_squared_sum = _mm256_add_ps(v_xi_squared, v_yi_squared);

        v_xyi_squared_sum = _mm256_cmp_ps(v_xyi_squared_sum, v_4f, _CMP_LT_OQ);
        test = _mm256_movemask_ps(v_xyi_squared_sum) & 255; // Doesn't take first 8 bits
        v_xyi_squared_sum = _mm256_and_ps(v_xyi_squared_sum, v_1f);
        v_iteration_counter = _mm256_add_ps(v_iteration_counter, v_xyi_squared_sum);

        v_xyi_squared_sum = _mm256_mul_ps(v_xi, v_yi);
        v_xi = _mm256_sub_ps(v_xi_squared,v_yi_squared);
        v_xi = _mm256_add_ps(v_xi,v_x0);
        v_yi = _mm256_add_ps(v_xyi_squared_sum,v_xyi_squared_sum);
        v_yi = _mm256_add_ps(v_yi,v_y0);

        iteration++;
      }
      
      float store[8];
      _mm256_store_ps(store, v_iteration_counter);

      int top = ((x_ + 7) < width) ? 8 : width & 7;
      for (int k = 0; k < top; k++)
      {
        iter_mem_buffer[x_ + k] = store[k];
        if (height -2 * y_ - 1)
          histogram[iter_mem_buffer[x_ + k]]++;
        histogram[iter_mem_buffer[x_ + k]]++;
      }

      // Increments by 8
      v_x_increment = _mm256_add_ps(v_4f, v_x_increment);
      v_x_increment = _mm256_add_ps(v_4f, v_x_increment);
    }
    v_line = _mm256_add_ps(v_1f, v_line);
    iter_mem_buffer += width;
  }

  int total = std::accumulate(histogram.begin(), histogram.end() - 1, 0);

  rgb8_t heat_mem[n_iterations + 1];
  float tmp = 0.0f;
  for (int i = 0; i < n_iterations; i++) {
    tmp += (float) histogram[i] / (float) total;
    heat_mem[i] = heat_lut(tmp);
  }
  
  iter_mem_buffer = &iter_mem[0];

  for (int y_ = 0; y_ < (height + 1) / 2; ++y_)
  {
    rgb8_t* lineptr = reinterpret_cast<rgb8_t*>(buffer);
    rgb8_t* lineptr1 = reinterpret_cast<rgb8_t*>(buffer + (height - 1 - 2 * y_) * stride);
    for (int x_ = 0; x_ < width; ++x_)
    {
      int iteration = iter_mem_buffer[x_];
      if (iteration < n_iterations) {
        lineptr[x_] = heat_mem[iteration];
        lineptr1[x_] = heat_mem[iteration];
      } else {
        lineptr[x_] = {0,0,0};
        lineptr1[x_] = {0,0,0};
      }
    }
    iter_mem_buffer += width;
    buffer += stride;
  }
}

void render_mt(std::byte* buffer,
               int width,
               int height,
               std::ptrdiff_t stride,
               int n_iterations)
{
  auto iter_mem = std::vector<int>((height + 1) / 2 * width);
  auto histogram = std::vector<int>(n_iterations + 1, 0);

  int* __restrict__ iter_mem_buffer = &iter_mem[0];

  // Round width mutliple of 8
  int roundedWidth = (width + 7) & ~7UL; 

  // Const declarations
  const float scale_x = 3.5f / (float) (width-1);
  const float scale_y = 2.0f / (float) (height-1);
  constexpr float x1 = -2.5f;
  constexpr float y1 = -1.0f;
  constexpr float increment = 1.0f;
  constexpr float comparison = 4.0f;

  // Const broadcast
  __m256 v_scale_x = _mm256_broadcast_ss(&scale_x);
  __m256 v_scale_y = _mm256_broadcast_ss(&scale_y);
  __m256 v_x1 = _mm256_broadcast_ss(&x1);
  __m256 v_y1 = _mm256_broadcast_ss(&y1);
  __m256 v_1f = _mm256_broadcast_ss(&increment);
  __m256 v_4f = _mm256_broadcast_ss(&comparison);

  float incr[8] = {0.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f};

  tbb::parallel_for(tbb::blocked_range<int>(0, (height + 1) / 2) ,[&] (const tbb::blocked_range<int> &r) {
  for (int y_ = r.begin(); y_ != r.end(); ++y_)
  {
    const float c_y = y_;
    __m256 v_mult_index = _mm256_broadcast_ss(&c_y);
    __m256 v_line = _mm256_mul_ps(v_1f, v_mult_index);
    __m256 v_x_increment = _mm256_load_ps(incr);

    for (int x_ = 0; x_ < roundedWidth; x_ += 8)
    {
      __m256 v_x0 = _mm256_mul_ps(v_scale_x, v_x_increment);
      v_x0 = _mm256_add_ps(v_x1, v_x0);
      __m256 v_y0 = _mm256_mul_ps(v_scale_y, v_line);
      v_y0 = _mm256_add_ps(v_y1, v_y0);

      __m256 v_iteration_counter = _mm256_xor_ps(v_scale_x, v_scale_x);
      __m256 v_xi = v_iteration_counter; // Initial xi = 0
      __m256 v_yi = v_iteration_counter; // Initial yi = 0

      int iteration = 0;
      unsigned int test = 1; // Non null value

      __m256 squared_y = _mm256_mul_ps(v_y0, v_y0);

      __m256 x_minus_quarter = _mm256_div_ps(v_1f, v_4f);
      x_minus_quarter = _mm256_sub_ps(v_x0, x_minus_quarter);

      __m256 tmp = _mm256_mul_ps(x_minus_quarter, x_minus_quarter);

      // Black circles detection
      __m256 q = _mm256_add_ps(tmp, squared_y);

      tmp = _mm256_add_ps(v_x0, v_1f);
      tmp = _mm256_mul_ps(tmp, tmp);
      tmp = _mm256_add_ps(tmp, squared_y);

      __m256 tmp1 = _mm256_mul_ps(v_4f, v_4f);
      tmp1 = _mm256_div_ps(v_1f, tmp1);

      __m256 detect = _mm256_cmp_ps(tmp, tmp1, _CMP_LT_OQ);
      detect = _mm256_and_ps(detect, v_1f);

      tmp = _mm256_add_ps(x_minus_quarter, q);
      tmp = _mm256_mul_ps(tmp, q);

      tmp1 = _mm256_div_ps(squared_y, v_4f);

      __m256 detect1 = _mm256_cmp_ps(tmp, tmp1, _CMP_LT_OQ);
      detect1 = _mm256_and_ps(detect1, v_1f);

      detect = _mm256_or_ps(detect, detect1);

      float detect_store[8];
      _mm256_store_ps(detect_store, detect);
      int mem = 0;
      for (int i = 0; i < 8; i++)
        mem += detect_store[i];
      
      const float float_iter = n_iterations;

      if (mem == 8)
      {
        v_iteration_counter = _mm256_mul_ps(_mm256_broadcast_ss(&float_iter), detect);
        iteration = n_iterations;
      }

      while (test != 0 && iteration < n_iterations)
      {
        __m256 v_xi_squared = _mm256_mul_ps(v_xi, v_xi);
        __m256 v_yi_squared = _mm256_mul_ps(v_yi, v_yi);

        __m256 v_xyi_squared_sum = _mm256_add_ps(v_xi_squared, v_yi_squared);

        v_xyi_squared_sum = _mm256_cmp_ps(v_xyi_squared_sum, v_4f, _CMP_LT_OQ);
        test = _mm256_movemask_ps(v_xyi_squared_sum) & 255; // Doesn't take first 8 bits
        v_xyi_squared_sum = _mm256_and_ps(v_xyi_squared_sum, v_1f);
        v_iteration_counter = _mm256_add_ps(v_iteration_counter, v_xyi_squared_sum);

        v_xyi_squared_sum = _mm256_mul_ps(v_xi, v_yi);
        v_xi = _mm256_sub_ps(v_xi_squared,v_yi_squared);
        v_xi = _mm256_add_ps(v_xi,v_x0);
        v_yi = _mm256_add_ps(v_xyi_squared_sum,v_xyi_squared_sum);
        v_yi = _mm256_add_ps(v_yi,v_y0);

        iteration++;
      }
      
      float store[8];
      _mm256_store_ps(store, v_iteration_counter);

      int top = ((x_ + 7) < width) ? 8 : width & 7;
      int tmp_offset = y_ * width + x_;

      for (int k = 0; k < top; k++)
      {
        iter_mem_buffer[tmp_offset + k] = store[k];
      }

      // Increments by 8
      v_x_increment = _mm256_add_ps(v_4f, v_x_increment);
      v_x_increment = _mm256_add_ps(v_4f, v_x_increment);
    }
  }});

  iter_mem_buffer = &iter_mem[0];
  for (int y_ = 0; y_ < (height + 1) / 2; y_++)
  {
    for (int x_ = 0; x_ < width; x_++)
    {
      if (height -2 * y_ - 1)
          histogram[iter_mem_buffer[x_]]++;
        histogram[iter_mem_buffer[x_]]++;
    }
    iter_mem_buffer += width;
  }

  int total = std::accumulate(histogram.begin(), histogram.end() - 1, 0);

  rgb8_t heat_mem[n_iterations + 1];
  float tmp = 0.0f;
  for (int i = 0; i < n_iterations; i++) {
    tmp += (float) histogram[i] / (float) total;
    heat_mem[i] = heat_lut(tmp);
  }
  
  iter_mem_buffer = &iter_mem[0];

  tbb::parallel_for(tbb::blocked_range<int>(0, (height + 1) / 2) ,[&] (const tbb::blocked_range<int> &r) {
  for (int y_ = r.begin(); y_ != r.end(); ++y_)
  {
    rgb8_t* lineptr = reinterpret_cast<rgb8_t*>(buffer + y_ * stride);
    rgb8_t* lineptr1 = reinterpret_cast<rgb8_t*>(buffer + (height - 1 - y_) * stride);
    for (int x_ = 0; x_ < width; ++x_)
    {
      int iteration = iter_mem_buffer[x_ + y_ * width];
      if (iteration < n_iterations) {
        lineptr[x_] = heat_mem[iteration];
        lineptr1[x_] = heat_mem[iteration];
      } else {
        lineptr[x_] = {0,0,0};
        lineptr1[x_] = {0,0,0};
      }
    }
  }});
}